import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { ProductProvider } from '../../providers/product/product';
import { Product } from '../../providers/product/productObject';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  products: any[] = [];

  constructor(public navCtrl: NavController, private provider: ProductProvider, private alertCtrl: AlertController) {
    this.getProducts();
  }

  private getProducts() {
    this.provider.read()
      .then((p: any[]) => {
        this.products = p;
      })
      .catch(() => {
        this.alertCtrl.create({ title: 'Falha ao ler produtos!', buttons: ['Ok'] }).present();
      })
  }

  newProduct() {
    this.navCtrl.push('ProductPage')
      .catch(() => {
        this.alertCtrl.create({ title: 'Tente novamente!', buttons: ['Ok'] }).present();
      })
  }

  editProduct(product: Product) {
    this.navCtrl.push('ProductPage', { product: product })
      .catch(() => {
        this.alertCtrl.create({ title: 'Tente novamente!', buttons: ['Ok'] }).present();
      })
  }

  removeProduct(product: Product) {
    this.provider.delete(product)
      .then(() => {

        var index = this.products.indexOf(product);
        this.products.splice(index, 1);

        this.alertCtrl.create({ title: 'Removido!', buttons: ['Ok'] }).present();
      })
      .catch(() => {
        this.alertCtrl.create({ title: 'Falha ao remover!', buttons: ['Ok'] }).present();
      })
  }
}
