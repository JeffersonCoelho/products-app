import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';
import { Product } from '../../providers/product/productObject';


@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  product: Product;
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private provider: ProductProvider) {

    this.product = new Product();

    if (this.navParams.data.id) {
      this.provider.read()
        .then((p: any) => {
          this.product = p;
        })
        .catch(() => {
          this.alertCtrl.create({ title: 'Tente novamente!', buttons: ['Ok'] }).present();
        })
    }

    this.setPageTitle();
  }

  submit() {
    this.checkAction()
      .then(() => {
        this.alertCtrl.create({ title: 'Salvo com sucesso!', buttons: ['Ok'] }).present();
        this.navCtrl.pop()
          .catch(() => {
            this.alertCtrl.create({ title: 'Tente novamente!', buttons: ['Ok'] }).present();
          })
      })
      .catch(() => {
        this.alertCtrl.create({ title: 'Falha ao salvar!', buttons: ['Ok'] }).present();
      });
  }

  private setPageTitle() {
    this.title = this.navParams.data.id ? 'Alterar produto' : 'Novo produto';
  }

  private checkAction() {
    if (this.product.id) {
      return this.provider.update(this.product);
    } else {
      return this.provider.create(this.product);
    }
  }

}
