import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { ProductProvider } from '../providers/product/product';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = null;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, productProvider: ProductProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();


      productProvider.createDB()
        .then(() => {
          this.homePage(splashScreen);
        })
        .catch(() => {
          this.homePage(splashScreen);
        });
    });
  }

  private homePage(ss: SplashScreen) {
    ss.hide();
    this.rootPage = HomePage;
  }
}

