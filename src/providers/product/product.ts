import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { Product } from './productObject';

@Injectable()
export class ProductProvider {

  constructor(private sqlite: SQLite) { }

  public create(product: Product) {
    return this.db()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO products (name, description) values (?, ?)';
        let data = [product.name, product.description];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public read() {
    return this.db()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM products';
        var data: any[];

        return db.executeSql(sql, data)
          .then((data: any) => {
            var size = data.rows.length;

            if (size > 0) {
              let products: any[] = [];
              for (var i = 0; i < size; i++) {

                var product = data.rows.item(i);
                products.push(product);
              }
              return products;
            } else {
              return [];
            }
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public update(product: Product) {
    return this.db()
      .then((db: SQLiteObject) => {
        let sql = 'UPDATE products SET name = ?, description = ? WHERE id = ?';
        let data = [product.name, product.description, product.id];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public delete(product: Product) {
    return this.db()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE FROM products WHERE id = ?';
        let data = [product.id];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public createDB() {
    return this.db()
      .then((db: SQLiteObject) => {

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS products (id integer primary key AUTOINCREMENT NOT NULL, name TEXT, description TEXT)']])
          .then(() => console.log('Tabelas criadas com sucesso!'))
          .catch(e => console.error('Tabelas não criadas, error: ', e));
      })
      .catch((e) => console.error(e));
  }

  private db() {
    return this.sqlite.create({
      name: 'products.db',
      location: 'default'
    })
      .catch((e) => console.error(e));
  }

}